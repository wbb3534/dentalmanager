package com.seop.dentalmanager.controller;

import com.seop.dentalmanager.model.CustomerInfoUpdateRequest;
import com.seop.dentalmanager.model.CustomerItem;
import com.seop.dentalmanager.model.CustomerRequest;
import com.seop.dentalmanager.model.CustomerResponse;
import com.seop.dentalmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();
        return result;
    }

    @GetMapping("/customer/id/{id}")
    public CustomerResponse getCustomer(@PathVariable long id) {
        CustomerResponse result = customerService.getCustomer(id);

        return  result;
    }

    @PutMapping("/info/id/{id}")
    public String putCustomerInfo(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdateRequest request) {
        customerService.putCustomerInfo(id, request);

        return "ok";
    }

    @PutMapping("/visit-date/id/{id}")
    public String pubCustomerVisitDate(@PathVariable long id) {
        customerService.putCustomerVisitDate(id);

        return "ok";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delCustomer(@PathVariable long id) {
        customerService.delCustomer(id);

        return "ok";
    }
}
