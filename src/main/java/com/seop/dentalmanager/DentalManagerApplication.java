package com.seop.dentalmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DentalManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DentalManagerApplication.class, args);
    }

}
