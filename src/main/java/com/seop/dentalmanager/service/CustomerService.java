package com.seop.dentalmanager.service;

import com.seop.dentalmanager.entity.Customer;
import com.seop.dentalmanager.model.CustomerInfoUpdateRequest;
import com.seop.dentalmanager.model.CustomerItem;
import com.seop.dentalmanager.model.CustomerRequest;
import com.seop.dentalmanager.model.CustomerResponse;
import com.seop.dentalmanager.repositoty.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerAge(request.getCustomerAge());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setCustomerBirth(request.getCustomerBirth());
        addData.setCustomerAddress(request.getCustomerAddress());
        addData.setResidentNum(request.getResidentNum());


        customerRepository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<Customer> originData = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for (Customer item : originData) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerNamePhone(item.getCustomerName() + "/" + item.getCustomerPhone());
            addItem.setCustomerBirthAge(item.getCustomerBirth() + "/" + item.getCustomerAge());
            addItem.setVisitLast(item.getVisitLast());

            String needVisitText = "";
            if (item.getVisitLast() == null) {
                needVisitText = "방문 불필요";
            } else if (item.getVisitLast().isBefore(item.getVisitLast().plusMonths(6))){
                needVisitText = "방문 필요";
            }

            addItem.setNeedVisit(needVisitText);

            String birthdayStateText = "";

            if (item.getCustomerBirth().getMonthValue() == LocalDate.now().getMonthValue() && item.getCustomerBirth()
                    .getDayOfMonth() == LocalDate.now().getDayOfMonth()) {
                birthdayStateText = "생일";
            } else {
                birthdayStateText = "생일아님";
            }
            addItem.setBirthdayState(birthdayStateText);

            result.add(addItem);
        }
        return result;
    }
    public CustomerResponse getCustomer(long id) {
        Customer origin = customerRepository.findById(id).orElseThrow();

        CustomerResponse result = new CustomerResponse();

        result.setCustomerName(origin.getCustomerName());
        result.setCustomerAddress(origin.getCustomerAddress());
        result.setCustomerPhone(origin.getCustomerPhone());
        result.setCustomerBirth(origin.getCustomerBirth());
        result.setCustomerAge(origin.getCustomerAge());
        result.setResidentNum(origin.getResidentNum());
        result.setVisitLast(origin.getVisitLast());

        return result;
    }

    public void putCustomerInfo(long id, CustomerInfoUpdateRequest request) {
        Customer origin = customerRepository.findById(id).orElseThrow();

        origin.setCustomerName(request.getCustomerName());
        origin.setCustomerPhone(request.getCustomerPhone());
        origin.setCustomerAddress(request.getCustomerAddress());

        customerRepository.save(origin);
    }

    public void putCustomerVisitDate(long id) {
        Customer origin = customerRepository.findById(id).orElseThrow();

        origin.setVisitLast(LocalDate.now());

        customerRepository.save(origin);
    }

    public void delCustomer(long id) {
        customerRepository.deleteById(id);
    }
}
