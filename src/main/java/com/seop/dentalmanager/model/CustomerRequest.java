package com.seop.dentalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Max(value = 100)
    private Integer customerAge;
    @NotNull
    @Length(min = 11, max = 13)
    private String customerPhone;
    @NotNull
    private LocalDate customerBirth;
    @NotNull
    @Length(min = 3, max = 30)
    private String customerAddress;
    @NotNull
    @Length(min = 13, max = 14)
    private String residentNum;
}
