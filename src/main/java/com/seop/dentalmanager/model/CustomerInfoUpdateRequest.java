package com.seop.dentalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerInfoUpdateRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 11, max = 13)
    private String customerPhone;
    @NotNull
    @Length(min = 3, max = 30)
    private String customerAddress;
}
