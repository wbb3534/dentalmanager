package com.seop.dentalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerItem {
    private Long id;
    private String customerNamePhone;
    private String customerBirthAge;
    private LocalDate visitLast;
    private String needVisit;
    private String birthdayState;
}
