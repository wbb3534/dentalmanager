package com.seop.dentalmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerResponse {
    private String customerName;
    private String residentNum;
    private String customerPhone;
    private String customerAddress;
    private Integer customerAge;
    private LocalDate visitLast;
    private LocalDate customerBirth;

}
