package com.seop.dentalmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 20)
    private String customerPhone;
    @Column(nullable = false, length = 30)
    private String customerAddress;
    @Column(nullable = false)
    private Integer customerAge;
    private LocalDate visitLast;
    @Column(nullable = false)
    private LocalDate customerBirth;
    @Column(nullable = false, length = 20)
    private String residentNum;
}
